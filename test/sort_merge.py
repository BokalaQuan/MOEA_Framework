#!/usr/bin/env python
# -*-encoding:utf-8-*-

def merge_sort(lst):
    if (len(lst) <= 1): return lst
    left = merge_sort(lst[:len(lst) / 2])
    right = merge_sort(lst[len(lst) / 2:len(lst)])
    result = []
    while len(left) > 0 and len(right) > 0:
        if (left[0] > right[0]):
            result.append(right.pop(0))
        else:
            result.append(left.pop(0))

    if (len(left) > 0):
        result.extend(merge_sort(left))
    else:
        result.extend(merge_sort(right))
    return result


def main():
    LL = [[12,32,1,23,55,2], [22, 11, 55, 33, 66]]

    for l in LL:

        l = merge_sort(l)

    print LL

if __name__ == "__main__":
    main()