from core.individual import Individual
from core.genetic_operator import fast_nondominated_sort, crowding_distance_sort
from core.parameter import POPULATION_SIZE, MAX_NUMBER_FUNCTION_EVAL, DIM
from core.util import save_pareto_solutions, func_trans_T, func_levy
from problem.mokp import MOKP
from nsga2.nsga2 import IndividualNSGA

import random
import copy
import matplotlib.pyplot as plot


class IndividualDA(IndividualNSGA):

    def __init__(self):
        super(IndividualDA, self).__init__()

        self.separation = []
        self.alignment = []
        self.cohesion = []
        self.attraction = []
        self.distraction = []

        self.step = []
        self.position = []

    def init_dragonfly(self):
        for x in range(DIM):
            self.separation.append(0)
            self.alignment.append(0)
            self.cohesion.append(0)
            self.attraction.append(0)
            self.distraction.append(0)

            if random.random() <= 0.5:
                self.position.append(1)
            else:
                self.position.append(0)

            if random.random() <= 0.5:
                self.step.append(1)
            else:
                self.step.append(0)

        self.chromosome = self.position
        self.fitness_evaluate()

    def update_dragonfly(self, neighbors, food, enemy, para):
        self.separation = []
        self.alignment = []
        self.cohesion = []
        self.attraction = []
        self.distraction = []

        for x in range(DIM):
            s = 0
            a = 0
            c = 0

            for y in range(len(neighbors)):
                # Separation
                s += neighbors[y].position[x] - self.position[x]
                # Alignment
                a += neighbors[y].step[x]
                # Cohesion
                c += neighbors[y].position[x]

            self.separation.append(-s)
            self.alignment.append(float(a) / len(neighbors))
            self.cohesion.append(float(c) / len(neighbors) - self.position[x])

            # Attraction to food
            self.attraction.append(food.position[x] - self.position[x])
            # Distraction from enemy
            self.distraction.append(enemy.position[x] + self.position[x])

        for x in range(DIM):
            self.step[x] = para['s'] * self.separation[x] + para['a'] * self.alignment[x] + \
                para['c'] * self.cohesion[x] + para['f'] * self.attraction[x] + \
                para['e'] * self.distraction[x] + para['w'] * self.step[x]

            if self.step[x] > 6:
                self.step[x] = 6
            elif self.step[x] < -6:
                self.step[x] = -6

            T = func_trans_T(self.step[x])

            if random.random() < T:
                self.position[x] = 1 - self.position[x]

        self.fitness_evaluate()



class MODA(object):

    def __init__(self):
        self.problem = MOKP(50, 2)
        self.current_poplist = []
        self.archive_poplist = []
        self.para = None

    def init_population(self):

        for x in range(POPULATION_SIZE):
            ind = IndividualDA()
            ind.init(self.problem.mokp_dict)
            ind.init_dragonfly()
            ind.fitness_evaluate()

            self.current_poplist.append(ind)


    def update_weight(self, gen):

        w = 0.9 - gen * (0.9 - 0.2) / MAX_NUMBER_FUNCTION_EVAL
        my_c = 0.1 - gen * (0.1 - 0) / (MAX_NUMBER_FUNCTION_EVAL / 2)

        if my_c < 0:
            my_c = 0

        if gen < (3 * MAX_NUMBER_FUNCTION_EVAL / 4):
            s = my_c
            a = my_c
            c = my_c
            f = 2 * random.random()
            e = my_c
        else:
            s = my_c / gen
            a = my_c / gen
            c = my_c / gen
            f = 2 * random.random()
            e = my_c / gen

        self.para = {'s': s,'a': a,'c': c,'f': f,'e': e,'w': w}

    def update_neighbor(self, ind):
        num = self.current_poplist.index(ind)
        neighbors = []
        for x in range(POPULATION_SIZE):
           if x != num:
               neighbors.append(self.current_poplist[x])

        return neighbors

    def update_archive(self):

        if not self.archive_poplist:
            archive_pre = fast_nondominated_sort(self.current_poplist)
            self.archive_poplist = copy.deepcopy(archive_pre[0])
        else:
            for ind_current in self.current_poplist:
                num = 0
                for ind_archive in self.archive_poplist:
                    if ind_archive.is_dominated(ind_current):
                        self.archive_poplist.remove(ind_archive)
                        break
                    elif ind_current.is_dominated(ind_archive) or ind_current.is_same_to(ind_archive):
                        num += 1

                if not num:
                    self.archive_poplist.append(copy.deepcopy(ind_current))

        num = 0
        print "Archive size = ", len(self.archive_poplist)
        # for ind in self.archive_poplist:
        #     print "ind ", num, ind.profit
        #     num += 1


    def main(self):
        self.init_population()
        self.update_archive()

        gen = 0
        while gen < MAX_NUMBER_FUNCTION_EVAL:
            print "Gen >>> ", gen
            self.update_weight(gen)

            for ind in self.current_poplist:
                ind_neighbor = self.update_neighbor(ind)
                ind.update_dragonfly(ind_neighbor, self.archive_poplist[0],
                                     self.archive_poplist[len(self.archive_poplist) - 1], self.para)

            self.update_archive()
            gen += 1

        poplist = fast_nondominated_sort(self.archive_poplist)

        # save_pareto_solutions(poplist[0], 'moda', 50, 2)
        #
        # x = []
        # y = []
        #
        # for ind in poplist[0]:
        #     x.append(ind.profit[0])
        #     y.append(ind.profit[1])
        #
        # plot.plot(x, y, 'o')
        # plot.show()

        return poplist[0]

if __name__ == '__main__':
    # al = MODA()
    # al.main()

    pareto_list_all = []

    for i in range(5):
        pareto_list = []
        al = MODA()
        pareto_list = al.main()
        pareto_list_all.extend(pareto_list)

    pareto_list_first_rank = fast_nondominated_sort(pareto_list_all)
    save_pareto_solutions(pareto_list_first_rank[0], 'moda', 50, 2)
    x = []
    y = []

    for ind in pareto_list_first_rank[0]:
        x.append(ind.profit[0])
        y.append(ind.profit[1])

    plot.plot(x, y, 'o')
    plot.show()