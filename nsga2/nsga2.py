from core.individual import IndividualKP
import core.genetic_operator as genetic
from core.parameter import PM,PC, POPULATION_SIZE, EXTERNAL_ARCHIVE_SIZE, MAX_NUMBER_FUNCTION_EVAL
from problem.mokp import MOKP
from core.util import save_pareto_solutions

import random
import json
import copy
import matplotlib.pyplot as plot

class IndividualNSGA(IndividualKP):

    def __init__(self):
        super(IndividualNSGA, self).__init__()

        self.num_dominated = 0
        self.dominating_list = []
        self.pareto_rank = 0
        self.crowding_distance = 0

    def copy(self):
        ind = IndividualNSGA()
        ind.problem = self.problem
        ind.chromosome = copy.copy(self.chromosome)
        ind.profit = copy.copy(self.profit)
        ind.weight = copy.copy(self.weight)
        ind.capacity = copy.copy(self.capacity)
        ind.dominate_state = self.dominate_state
        ind.num_dominated = self.num_dominated
        ind.dominating_list = []
        ind.pareto_rank = self.pareto_rank
        ind.crowding_distance = self.crowding_distance
        return ind

    def clear_property(self):
        self.num_dominated = 0
        self.dominating_list = []
        self.pareto_rank = 0
        self.crowding_distance = 0

    def print_individual(self):
        print "ind profit", self.profit


class NSGA2(object):

    def __init__(self):
        self.problem = MOKP(50, 2)
        self.current_poplist = []
        self.pre_poplist = []

    def init_population(self):

        for i in xrange(POPULATION_SIZE):
            ind = IndividualNSGA()
            ind.init(self.problem.mokp_dict)
            ind.init_chromosome(50)
            ind.fitness_evaluate()

            self.current_poplist.append(ind)

    def copy_current_to_pre(self):
        self.pre_poplist = []
        self.pre_poplist = copy.deepcopy(self.current_poplist)

    def make_new_population(self):
        union_population = []
        union_population.extend(self.current_poplist)
        union_population.extend(self.pre_poplist)

        print "<<<<<<<<<<<<<<<<<<<<", len(union_population)

        pareto_rank_set_list = genetic.fast_nondominated_sort(union_population)

        length = 0
        for x in pareto_rank_set_list:
            length += len(x)

        print ">>>>>>>>>>>>>>>>>>>>", length

        genetic.crowding_distance_sort(pareto_rank_set_list)

        new_pop = []
        for ll in pareto_rank_set_list:
            new_pop.extend(ll)

        self.current_poplist = []

        for x in range(POPULATION_SIZE):
            self.current_poplist.append(new_pop[x])

    def evolution(self):
        self.current_poplist = []

        for x in range(POPULATION_SIZE):
            index_ind0 = 0
            index_ind1 = 0

            while index_ind0 == index_ind1:
                index_ind0 = random.randint(0, POPULATION_SIZE - 1)
                index_ind1 = random.randint(0, POPULATION_SIZE - 1)

            if self.pre_poplist[index_ind0].pareto_rank < self.pre_poplist[index_ind1].pareto_rank:
                self.current_poplist.append(self.pre_poplist[index_ind0].copy())
            elif self.pre_poplist[index_ind0].pareto_rank == self.pre_poplist[index_ind1].pareto_rank and \
                self.pre_poplist[index_ind0].crowding_distance > self.pre_poplist[index_ind1].crowding_distance:
                self.current_poplist.append(self.pre_poplist[index_ind0].copy())
            else:
                self.current_poplist.append(self.pre_poplist[index_ind1].copy())


        for x in xrange(POPULATION_SIZE / 2):
            self.current_poplist[x].mating(self.current_poplist[POPULATION_SIZE - 1 - x])


    def main(self):
        self.init_population()
        self.copy_current_to_pre()

        gen = 0

        while gen < MAX_NUMBER_FUNCTION_EVAL:
            print "Gen >>> ", gen
            self.make_new_population()
            self.copy_current_to_pre()
            self.evolution()
            gen += 1


        for ind in self.pre_poplist:
            if not ind.pareto_rank:
                self.pre_poplist.remove(ind)

        return self.pre_poplist

        # save_pareto_solutions(self.pre_poplist, 'nsga', 50, 2)
        #
        # x = []
        # y = []
        #
        # for ind in self.pre_poplist:
        #     x.append(ind.profit[0])
        #     y.append(ind.profit[1])
        #
        # plot.plot(x, y, 'o')
        # plot.show()

if __name__ == '__main__':

    pareto_list_all = []

    for i in range(5):
        pareto_list = []
        al = NSGA2()
        pareto_list = al.main()
        pareto_list_all.extend(pareto_list)

    pareto_list_first_rank = genetic.fast_nondominated_sort(pareto_list_all)
    save_pareto_solutions(pareto_list_first_rank[0], 'nsga', 50, 2)
    x = []
    y = []

    for ind in pareto_list_first_rank[0]:
        x.append(ind.profit[0])
        y.append(ind.profit[1])

    plot.plot(x, y, 'o')
    plot.show()