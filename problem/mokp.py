import networkx as nx
import json
import random
import os

PATH = '/home/bokala/PycharmProjects/MOEA_Framework/problem/'

class MOKP(object):
    '''
        class Knap
    '''
    item_weight = []
    item_profit = []
    item_ratio = []
    knap_capacity = []


    num_item = 0
    num_knap = 0

    mokp_dict = []

    def __init__(self, num_item, num_knap):

        self.num_item = num_item
        self.num_knap = num_knap

        self.get_problem()
        # self.creat_problem()

    def get_problem(self):
        FILE_LOAD = PATH + str(self.num_item) + '_' + str(self.num_knap) + '.json'

        with open(FILE_LOAD, 'r') as f:
            conf = json.load(f)

            for knap in conf:
                self.mokp_dict.append(knap)


    def creat_problem(self):

        for x in range(self.num_knap):
            sum = 0
            weight_list = []
            profit_list = []
            ratio_list = []

            for i in range(self.num_item):
                weight = int(random.uniform(1, 100))
                profit = int(random.uniform(1, 100))

                weight_list.append(weight)
                profit_list.append(profit)
                ratio_list.append(float('%.2f' % (float(profit) / weight)))

                sum += weight

            self.item_weight.append(weight_list)
            self.item_profit.append(profit_list)
            self.item_ratio.append(ratio_list)
            self.knap_capacity.append(sum / 2)


        for x in range(2):
            item_dict = self.to_dict_item(self.item_weight[x], self.item_profit[x], self.item_ratio[x])
            knap_dict = self.to_dict_knap(x + 1, item_dict, self.knap_capacity[x])

            self.mokp_dict.append(knap_dict)

        print self.mokp_dict

        self.save_problem()

    def save_problem(self):
        FILE_PATH = PATH + "\\" + str(self.num_item) + "_" + str(self.num_knap) + ".json"

        with open(FILE_PATH, 'w') as f:

            f.write(json.dumps(self.mokp_dict, indent=4, sort_keys=True))

            print "File OK."

    @staticmethod
    def to_dict_knap(index_knapsack, item, capacity):

        return {'knapsack': index_knapsack,
                'capacity': capacity,
                'items': item}

    @staticmethod
    def to_dict_item(item_weight, item_profit, item_ratio):

        item_list = []
        for x in range(len(item_profit)):
            item = {}
            item['item'] = x
            item['property'] = {'profit': item_profit[x],
                                'weight': item_weight[x],
                                'ratio': item_ratio[x]}

            item_list.append(item)

        return item_list


if __name__ == '__main__':

    mokp = MOKP(50, 2)
    mokp.creat_problem()












