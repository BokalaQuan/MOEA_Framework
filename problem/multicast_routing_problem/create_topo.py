import json
import xlrd
import os
import random

FILE_PATH = os.path.split(os.path.realpath(__file__))[0]
XLS_PATH = FILE_PATH + '/topo_file/topo_200_524.xls'
SWITCH_PATH = FILE_PATH + '/topo_file/topo8/switch_info.json'
LINK_PATH = FILE_PATH + '/topo_file/topo8/link_info.json'

def init_topo(num_node, num_edge):
    node_list = []
    edge_list = []

    PATH = XLS_PATH

    data = xlrd.open_workbook(PATH)
    table = data.sheet_by_index(0)

    for i in xrange(1, num_node + 1):
        sw = {"dpid": i,
              "attribute": "edge",
              "name": get_switch_dpid(i)}
        node_list.append(sw)

    for i in range(table.nrows):
        str = int(table.cell(i, 0).value)
        dst = int(table.cell(i, 1).value)
        temp = random.uniform(100, 1000)
        delay = int(temp)
        temp = random.uniform(0.001, 0.1)
        loss = float('%.3f' % temp)
        bandwidth = random.randint(1, 20) * 10

        link = {"src": str+1,
                "dst": dst+1,
                "delay": delay,
                "loss": loss,
                "bandwidth": bandwidth}
        edge_list.append(link)

    with open(SWITCH_PATH, 'w') as json_file:
        pfp = json.dumps(node_list, ensure_ascii=True, indent=4, sort_keys=True)
        json_file.write(pfp)

    with open(LINK_PATH, 'w') as json_file:
        pfp = json.dumps(edge_list, ensure_ascii=True, indent=4, sort_keys=True)
        json_file.write(pfp)




def get_switch_dpid(number):
    prefix = "00"
    if number >= int(100):
        prefix = ""
    elif number >= int(10):
        prefix = "0"

    return 's' + prefix + str(number)


def get_host_dpid(number):
    prefix = "00"
    if number >= int(100):
        prefix = ""
    elif number >= int(10):
        prefix = "0"

    return 'h' + prefix + str(number)




if __name__ == '__main__':
    init_topo(200, 524)




