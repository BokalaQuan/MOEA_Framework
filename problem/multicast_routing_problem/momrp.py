import networkx as nx
import json
import random
import os

FILE_PATH = os.path.split(os.path.realpath(__file__))[0]
TOPO_PATH = FILE_PATH + '/topo_file/'

class MRP(object):
    '''
    class Multicast Routing Problem 
    '''

    def __init__(self):
        self.switches = []
        self.links = []

        self.num_switch = 0
        self.num_link = 0

    def initialize(self, filename):
        SWITCH_PATH = TOPO_PATH + filename + '/switch_info.json'
        LINK_PATH = TOPO_PATH + filename + '/link_info.json'

        with open(SWITCH_PATH, 'r') as f:
            conf = json.load(f)

            for sw in conf:
                self.switches.append(sw)
                self.num_switch += 1

        with open(LINK_PATH, 'r') as f:
            conf = json.load(f)

            for link in conf:
                self.links.append(link)
                self.num_link += 1

    def get_problem(self):
        return self.switches, self.links, self.num_switch, self.num_link