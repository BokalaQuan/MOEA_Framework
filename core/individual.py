import random
import copy
import json
import networkx as nx

from problem.mokp import MOKP
from problem.multicast_routing_problem.momrp import MRP
from parameter import PM, PC

INF = 99999.9

class IndividualKP(object):

    def __init__(self):
        self.problem = None

        self.chromosome = []

        self.profit = []
        self.weight = []
        self.capacity = []

        self.dominate_state = False

    def init(self, mokp):
        self.problem = mokp

    def copy(self):
        pass

    def init_chromosome(self, length):

        for i in range(length):
            if random.random() <= 0.5:
                self.chromosome.append(1)
            else:
                self.chromosome.append(0)

    def fitness_evaluate(self):
        self.profit = []
        self.weight = []
        self.capacity = []

        for y in xrange(2):
            sum_weight = 0
            sum_profit = 0
            for x in xrange(self.chromosome.__len__()):

                if self.chromosome[x]:
                    sum_weight += self.problem[y]['items'][x]['property']['weight']
                    sum_profit += self.problem[y]['items'][x]['property']['profit']

            self.capacity.append(self.problem[y]['capacity'])

            self.profit.append(sum_profit)
            self.weight.append(sum_weight)

        self.greedy_repair_heuristic()


    def is_dominated(self, ind):
        if self.profit[0] <= ind.profit[0] and self.profit[1] < ind.profit[1]:
            return True
        elif self.profit[0] < ind.profit[0] and self.profit[1] <= ind.profit[1]:
            return True
        return False

    def is_same_to(self, ind):
        for x in xrange(2):
            if self.profit[x] != ind.profit[x]:
                return False

        return True

    def is_feasible(self):
        for x in xrange(self.weight.__len__()):
            if self.weight[x] > self.capacity[x]:
                return False

        return True

    def _uniform_crossover(self, ind):
        chrom = copy.copy(self.chromosome)

        for x in range(self.chromosome.__len__()):
            if random.random() < 0.5:
                self.chromosome[x] = ind.chromosome[x]
                ind.chromosome[x] = chrom[x]

    def _single_point_crosser(self, ind):
        chrom = copy.copy(self.chromosome)
        index_cro = random.randint(0, len(self.chromosome))

        for index in xrange(index_cro, len(self.chromosome)):
            self.chromosome[index] = ind.chromosome[index]
            ind.chromosome[index] = chrom[index]

    def multiple(self):
        for x in xrange(len(self.chromosome)):
            if random.random() < PM:
                self.chromosome[x] = 1 - self.chromosome[x]

    def mating(self, ind):
        if random.random() < PM:
            if random.random() < 0.5:
                self._single_point_crosser(ind)
            else:
                self._uniform_crossover(ind)

        self.multiple()
        self.fitness_evaluate()
        ind.multiple()
        ind.fitness_evaluate()


    def greedy_repair_heuristic(self):

        while not self.is_feasible():
            for iKnap in xrange(2):
                if self.weight[iKnap] > self.capacity[iKnap]:
                    min_ratio = 999.9
                    id_item = []
                    delete_id = 0

                    for index in xrange(len(self.chromosome)):
                        if self.chromosome[index]:
                            id_item.append(index)
                            if min_ratio > self.problem[iKnap]['items'][index]['property']['ratio']:
                                min_ratio = self.problem[iKnap]['items'][index]['property']['ratio']
                                delete_id = self.problem[iKnap]['items'][index]['item']

                    self._delete_item(delete_id)

    def _delete_item(self, index):
        self.chromosome[index] = 0

        for x in xrange(2):
            self.profit[x] -= self.problem[x]['items'][index]['property']['profit']
            self.weight[x] -= self.problem[x]['items'][index]['property']['weight']

    def clear_property(self):
        pass


class IndividualMRP(object):
    def __init__(self):
        self.src_node = None
        self.dst_node = []

        self.edge_list = []
        self.edge_all_list = []

        self.chromosome = []
        self.paths = []

        self.delay = 0.0
        self.loss = 0.0
        self.bandwidth = float('Infinity')

        self.fitness = 0.0

        self.graph = nx.Graph()

        self.dominated = False
        self.pareto_rank = 0
        self.crowding_distance = 0
        self.num_dominated = 0
        self.location = 0
        self.dominating_list = []

    def initialize(self, len_chrom, edge_all_list, src, dst):
        #
        for i in range(len_chrom):
            if random.random() < 0.5:
                self.chromosome.append(True)
            else:
                self.chromosome.append(False)

        self.edge_all_list = edge_all_list
        self.src_node = src
        self.dst_node = dst

        # evaluate this individual and computing it's fitness or property
        self.fitness_evaluate()


    def fitness_evaluate(self):
        self._init_subgraph_by_chromosome()

    def _init_subgraph_by_chromosome(self):
        edge_select = []

        state = True

        for x in range(self.chromosome.__len__()):
            if self.chromosome[x]:
                edge_select.append(self.edge_all_list[x])

        for edge in edge_select:
            self.graph.add_edge(edge.src, edge.dst, delay=edge.delay, band=edge.band, loss=edge.loss)


        if state:
            if self.src_node not in self.graph.node:
                state = False

            else:
                for dst in self.dst_node:
                    if dst not in self.graph.node:
                        state = False
                        break

        if state:
            if nx.is_connected(self.graph):
                state = True
            else:
                state = False

        if state:
            self._init_tree_by_subgraph()
        else:
            self.delay = INF
            self.loss = INF

    def _init_tree_by_subgraph(self):
        self.paths = []

        for dst in self.dst_node:
            if random.random() < 0.5:
                path = nx.dijkstra_path(self.graph, self.src_node, dst, weight='delay')
            else:
                path = nx.dijkstra_path(self.graph, self.src_node, dst, weight='loss')

            self.paths.append(path)

        self._cal_fitness()


    def _cal_fitness(self):

        max_delay = 0.0
        max_loss = 0.0

        for path in self.paths:
            delay = 0.0
            loss = 1.0
            for index in range(len(path) - 1):
                delay += self.graph.edge[path[index]][path[index + 1]]['delay']
                loss *= 1 - self.graph.edge[path[index]][path[index + 1]]['loss']
                if self.bandwidth > self.graph.edge[path[index]][path[index + 1]]['band']:
                    self.bandwidth = self.graph.edge[path[index]][path[index + 1]]['band']

            # if max_delay < delay:
            #     max_delay = delay
            #
            # if max_loss < 1 - loss:
            #     max_loss = 1 - loss

            max_delay += delay
            max_loss += 1 - loss


        self.delay = float(max_delay / len(self.dst_node))
        self.loss = float(max_loss) / len(self.dst_node)


    def is_better_than(self, ind):
        if self.loss <= ind.loss and self.delay < ind.delay:
            return True
        elif self.loss < ind.loss and self.delay <= ind.delay:
            return True
        else:
            return False

    def is_same_to(self, ind):
        if self.delay == ind.delay and self.loss == ind.loss:
            return True
        return False

    def single_point_crossover(self, ind):
        length = self.chromosome.__len__()
        pos = random.randint(0, length)

        chrom_copy = copy.copy(self.chromosome)

        for i in range(length):
            if i > pos:
                self.chromosome[i] = ind.chromosome[i]
                ind.chromosome[i] = chrom_copy[i]

            if random.random() < 0.08:
                if self.chromosome[i]:
                    self.chromosome[i] = False
                else:
                    self.chromosome[i] = True

                if ind.chromosome[i]:
                    ind.chromosome[i] = False
                else:
                    ind.chromosome[i] = True

        self.fitness_evaluate()
        ind.fitness_evaluate()

    def uniform_crossover(self, ind):
        length = self.chromosome.__len__()

        chrom_copy = copy.copy(self.chromosome)

        for i in range(length):
            if random.random() < 0.5:
                self.chromosome[i] = ind.chromosome[i]
                ind.chromosome[i] = chrom_copy[i]
            if random.random() < 0.08:
                if self.chromosome[i]:
                    self.chromosome[i] = False
                else:
                    self.chromosome[i] = True

                if ind.chromosome[i]:
                    ind.chromosome[i] = False
                else:
                    ind.chromosome[i] = True

        self.fitness_evaluate()
        ind.fitness_evaluate()


    def clear_property(self):
        self.dominated = False
        self.pareto_rank = 0
        self.crowding_distance = 0
        self.num_dominated = 0
        self.location = 0
        self.dominating_list = []

    def to_format(self):
        r = {"paths": self.paths,
             "delay": self.delay,
             "loss": self.loss,
             "bandwidth": self.bandwidth}
        return r

    @staticmethod
    def set_to_format(sets):
        # for item in sets:
        #     r.append(item.to_format())
        #     r["individual"] = item.to_format()
        # return r

        r = [{"Individual": item,
              "attribute": sets[item].to_format()} for item in range(len(sets))]
        # for item in range(len(sets)):
        return r