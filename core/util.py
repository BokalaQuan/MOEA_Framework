import json
import math
import random

PATH = 'C:\\Users\\quanw\\PycharmProjects\\MOEA_MKP\\solutions\\'

def save_pareto_solutions(solutions, algorithm, num_item, num_knap):
    FILE = PATH + algorithm + '_' + str(num_item) + '_' + str(num_knap) + '.json'

    json_solution = to_dict_solutions(solutions)

    with open(FILE, 'w') as f:
        f.write(json.dumps(json_solution, indent=4, sort_keys=True))

        print "Save PS OK."


def to_dict_solutions(solutions):
    solutions_dict = []

    num = 1
    for solution in solutions:
        sl = {
              'profit': solution.profit,
              'pareto_rank': solution.pareto_rank,
              'crowding_distance': solution.crowding_distance}

        sl_dict = {'solution': num,
                   'property': sl}

        solutions_dict.append(sl_dict)

    return solutions_dict

def func_trans(x):
    func = 1 / (1 + math.exp(-x))
    return func

def func_trans_T(x):
    func = abs(x / (math.sqrt(math.pow(x, 2)) + 1))
    return func


def func_levy(d):
    beta = float(3 / 2)
    pi = math.pi

    theta = (math.gamma(1 + beta) * math.sin(pi * beta / 2) / (math.gamma((1 + beta) / 2) * beta * math.pow(2, ((beta - 1) / 2))))
    sigma = math.pow(theta, (1 / beta))

    r1 = random.random()
    r2 = random.random()

    step = 0.01 * r1 * sigma / math.pow(r2, (1 / beta))
    return step
