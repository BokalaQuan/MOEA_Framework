from nsga2.nsga2 import IndividualNSGA
from core.parameter import PM,PC, POPULATION_SIZE, EXTERNAL_ARCHIVE_SIZE, MAX_NUMBER_FUNCTION_EVAL
from problem.mokp import MOKP
from core.util import save_pareto_solutions, func_trans
import core.genetic_operator as genetic

import random
import copy
import math
import matplotlib.pyplot as plot

class IndividualS(IndividualNSGA):

    def __init__(self):
        super(IndividualS, self).__init__()

        self.mofitness = None
        self.position = []

    def init_position(self):
        for i in self.chromosome:
            self.position.append(0)


    def update_leader(self, food, gen):
        c1 = 2 * math.exp(-math.pow(4 * gen / MAX_NUMBER_FUNCTION_EVAL, 2))
        c2 = random.random()
        for x in range(self.position.__len__()):
            if random.random() < 0.5:
                self.position[x] = food.position[x] + c1 * c2
            else:
                self.position[x] = food.position[x] - c1 * c2

        print "position >>>>>>>>>>> ", self.position

        self.update_chromosome()
        self.fitness_evaluate()

    def update_follower(self, ind):
        for x in range(self.position.__len__()):
            self.position[x] = (self.position[x] + ind.position[x]) / 2

        self.update_chromosome()
        self.fitness_evaluate()

    def update_chromosome(self):
        self.chromosome = []

        for x in self.position:
            if random.random() > func_trans(x):
                self.chromosome.append(1)
            else:
                self.chromosome.append(0)

    def computing_mofitness(self):
        self.mofitness = 1 / (math.pow(2, self.pareto_rank) + 1 / (1 + self.crowding_distance))


class MSSA(object):

    def __init__(self):
        self.problem = MOKP(50, 2)
        self.current_poplist = []
        self.external_poplist = []

    def init_population(self):

        for i in xrange(POPULATION_SIZE):
            ind = IndividualS()
            ind.init(self.problem.mokp_dict)
            ind.init_chromosome(50)
            ind.init_position()
            ind.fitness_evaluate()

            self.current_poplist.append(ind)

    def init_external(self):
        poplist = genetic.fast_nondominated_sort(self.current_poplist)

        self.external_poplist.extend(poplist[0])


    def update_external(self, poplist):

        union_list = []
        union_list.extend(self.external_poplist)
        union_list.extend(poplist)

        end_list = genetic.fast_nondominated_sort(union_list)
        # genetic.crowding_distance_sort(end_list)

        self.external_poplist = []
        for ind in end_list[0]:
            self.external_poplist.append(ind)

        # for ll in end_list:
        #     for ind in ll:
        #         ind.computing_mofitness()
        #         new_list.append(ind)

        # genetic.object_shell_sort(new_list, 'mofitness')

        # self.external_poplist = []
        # for x in xrange(POPULATION_SIZE):
        #     self.external_poplist.append(new_list[x])


    def update_population(self, gen):

        food = self.external_poplist[random.randint(0, len(self.external_poplist) - 1)]

        self.current_poplist[0].update_leader(food, gen)

        for x in xrange(1, POPULATION_SIZE):
            self.current_poplist[x].update_follower(self.current_poplist[x - 1])


    def main(self):
        self.init_population()
        self.init_external()

        gen = 0
        while gen < MAX_NUMBER_FUNCTION_EVAL:
            print "Gen >>>>>>>>>> ", gen
            self.update_population(gen)
            self.update_external(self.current_poplist)
            gen += 1

        save_pareto_solutions(self.external_poplist, 'mssa', 50, 2)
        x = []
        y = []

        for ind in self.external_poplist:
            x.append(ind.profit[0])
            y.append(ind.profit[1])

        plot.plot(x, y, 'o')
        plot.show()

if __name__ == '__main__':
    al = MSSA()
    al.main()
